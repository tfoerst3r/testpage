---
title: Home
---

<!------------------->
<!----- CHAPTER ----->
<!------------------->
# Testing pyscript

<!-- pyscript routines -->

<py-config src="../pyscript.toml"></py-config>
<py-script src="../mycode/main.py"></py-script>


<!-- PAGE -->
Translate English into Pirate<br>
<input type="text" id="english" placeholder="Type your English in here ..."><br>
[Translate!](#){ .md-button .md-button--primary id="english" style="padding: 0.25rem; margin-top: 0.5rem; text-align: center;" py-click="english_to_pirate()" onclick="clearBox('output')"  }

<div id="output"></div>
<!-- END -->
